<?php

/**
 * Implements hook_drush_cache_clear().
 */
function opcache_drush_cache_clear(&$types) {
  $types['opcache'] = 'drush_opcache_reset';
}

/**
 * Implements hook_drush_command().
 */
/*
function opcache_drush_command() {
  return array(
    'opcache-reset' => array(
      'description' => 'Resets the OPcache cache in the web context.',
    ),
  );
}
*/

/**
 * Callback to clear the OPcache cache.
 *
 * We have to do a stupid trick here to trigger a call to opcache_reset() from
 * within the web server's PHP environment, because it does not share a cache
 * with the CLI environment.
 *
 * One of the many reasons that this is less than ideal is that we end up
 * populating some of the Drupal caches that might have just been emptied in
 * the process of 
 */
function drush_opcache_reset() {
  global $base_url;
  if (preg_match('/default$/', $base_url)) {
    drush_log(dt('In order to properly reset the OPcache cache, please use the -l/--uri flag to specify the correct URL of this Drupal installation.'), 'error');
    return;
  }
  // We can't use plain drupal_get_token() here because it requires that the
  // session_id() be consistent; it won't be between the web and CLI contexts.
  $token = drupal_hmac_base64('opcache-reset:' . REQUEST_TIME, drupal_get_private_key() . drupal_get_hash_salt());
  $url = url('opcache-reset/' . REQUEST_TIME . '/' . $token, array('absolute' => TRUE));
  $response = drupal_http_request($url);
  switch ($response->code) {
    case 200:
      drush_log(dt('OPcache was successfully reset.'), 'success');
      break;
    case 404:
      drush_log(dt('OPcache was not successfully reset as the reset path could not be found. Check that the URL provided to the -l/--uri flag is correct, or try running `drush cc menu`.'), 'error');
      break;
    case 403:
      drush_log(dt('OPcache was not successfully reset because making the request resulted in an Access Denied error. This may happen if too much time elapsed during the request process. Please try again.'), 'error');
      break;
    case 0:
      // This can happen when the call to stream_socket_client() inside of
      // drupal_http_request() fails so spectacularly that it doesn't even set
      // the $errno parameter. The actual problem might be that the domain name
      // could not be resolved.
      drush_log(dt('OPcache was not successfully reset because the request could not be properly initiated. Check that the URL provided to the -l/--uri flag is correct.'), 'error');
      break;
    default:
      drush_log(dt('OPcache was not successfully reset. The request resulted in an error/status code of @code.', array('@code' => $response->code)), 'error');
  }
}

/**
 * Implements… actually, I'm not sure what this implements.
 *
 * In the case of a `drush cc all`, Drush will clear its own cache, then tell
 * Drupal to clear all its caches - in the CLI context. Our implementation of
 * hook_flush_caches() will not even bother if drupal_is_cli() == TRUE. However,
 * Drush will invoke drush_[all_modules]_cache_clear(), so we'll use an
 * implementation of that to see if the "all" cache was cleared, and reset the
 * OPcache if that's the case. This should be the least surprising behavior.
 */
function drush_opcache_cache_clear($type = NULL) {
  if ($type === 'all') {
    drush_opcache_reset();
  }
}
